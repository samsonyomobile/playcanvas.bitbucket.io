ASSET_PREFIX = "";
SCRIPT_PREFIX = "";
SCENE_PATH = "1327677.json";
CONTEXT_OPTIONS = {
    'antialias': true,
    'alpha': true,
    'preserveDrawingBuffer': false,
    'preferWebGl2': true,
    'powerPreference': "default"
};
SCRIPTS = [ 66577402, 66577409, 66577413, 66577401, 66578817, 66588886, 66602609, 66608720, 66614540, 67078564, 67080849, 67085463, 67085465, 67086620, 67202677, 67227997, 67227998, 67233083, 67234158, 67234200, 67295010, 67301601, 67303180, 67343485, 67547875, 67547932, 67548595 ];
CONFIG_FILENAME = "config.json";
INPUT_SETTINGS = {
    useKeyboard: true,
    useMouse: true,
    useGamepads: false,
    useTouch: true
};
pc.script.legacy = false;
PRELOAD_MODULES = [
    {'moduleName' : 'Ammo', 'glueUrl' : 'files/assets/67078366/1/ammo.wasm.js', 'wasmUrl' : 'files/assets/67078367/1/ammo.wasm.wasm', 'fallbackUrl' : 'files/assets/67078365/1/ammo.js', 'preload' : true},
];
